# Ms Pacman

![Ms Pacman](https://www.gameogre.com/reviewdirectory/upload/ms-pacman.jpg)

A library for building web resources from a config file. Given a config, this library has functions for

- compiling resources
- write to object (for serving from memory)
- write to file system

```
yarn add git+ssh://git@gitlab.cbancnetwork.com:libs/ms-pacman.git --dev
```

## Build an app to folder

Build resources, bind into html, then write the resulting buffers to the file system. This is good for build systems that compile to static files servers.

```
const { buildAll, writeFilesToFileSystem } = require('ms-pacman');
const build_steps = [...];
const html_steps = [...];

buildAll(build_steps, html_steps)
  .then((files) => writeFilesToFileSystem(files))
  .then(() => {
    console.log('Congratulations! Make was successful.');
    process.exit();
  })
  .catch((e) => {
    console.error(e);
    process.exit(1);
  });
```

## Build an app to object

Build resources, bind into html, then transform the resulting buffers to an object where the key is the application pathname and the value is a Buffer with the bits for the resource. This is good for serving files from memory.

```
const { buildAll, writeFilesToObject } = require('ms-pacman');
const build_steps = [...];
const html_steps = [...];

buildAll(build_steps, html_steps)
  .then((files) => writeFilesToFileSystem(files))
  .then(() => {
    console.log('Congratulations! Make was successful.');
    process.exit();
  })
  .catch((e) => {
    console.error(e);
    process.exit(1);
  });
```

## Top Level Config

```js
{
  static_target: path.resolve('./dist'),
  public_path: '/some-cool-app',
  html_steps: [ .. see next section .. ],
  build_steps: [ .. see next section .. ]
};
```

# What is a build step?

A build step takes a config, then turns it into a compiled resource.

#### Elm

Note: `debug` and `optimize` are mutually exclusive where `debug` takes precendent. i.e. if `debug === true`, then `optimize` is ignored.

```
{
  type: 'elm',
  options: {
    entry: path.resolve('./src/elm/Main.elm'),
    output: 'inline',
    name: 'main',
    target: './dist',
    public_path: '/some-cool-app',
    optimize: false,
    debug: true
  }
}
```

#### JavaScript

```
{
  type: 'js',
  options: {
    output: 'inline',
    entry: path.resolve('./src/static/index.js'),
    name: 'index',
    target: './dist',
    public_path: '/some-cool-app',
    hash: true
  }
}
```

- hash: when true, the md5 of the content is inserted into the filename like `index-{md5}.js`
- output: determines where the output of the step is sent.
  - `inline`: the content is written directly to the html in a script tag
  - 'reference': the output is written to the file system and a script tag is added to the html
  - 'static': the output is written to the file system and nothing is added to the html

#### Sass

```
{
  type: 'sass',
  options: {
    inline: true,
    entry: path.resolve('./src/static/styles/main.scss'),
    name: 'main',
    target: './dist',
    public_path: '/some-cool-app',
    includePaths: [ path.resolve('./src/static/styles'), path.resolve('./node_modules') ]
  }
}
```

- output: determines where the output of the step is sent.
  - `inline`: the content is written directly to the html in a style tag
  - 'reference': the output is written to the file system and a link tag is added to the html
  - 'static': the output is written to the file system and nothing is added to the html

#### Copy

```
{
  type: 'copy',
  options: {
    entry: path.resolve('./src/static/index.js'),
    filename: 'index',
    target: './dist',
    public_path: '/some-cool-app'
  }
}
```

### What is an html step?

An html step takes an html template and embed resources (css or script). The embed can either be a reference or inline.

```
{
  entry: path.resolve('./src/static/index.html'),
  target: './dist',
  name: 'index'
}
```
