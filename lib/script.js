const path = require('path');
const { promisify } = require('util');
const readFile = promisify(require('fs').readFile);
const unlink = promisify(require('fs').unlink);
const exists = promisify(require('fs').exists);
const exec = promisify(require('child_process').exec);
const mkdirp = promisify(require('mkdirp'));
const UglifyJS = require('uglify-js');
const { fileName } = require('./shared.js');
const { Validator } = require('jsonschema');
const v = new Validator();

const minifyJs = async (entry) => {
  const src = await readFile(entry);
  const { code, error } = UglifyJS.minify(src.toString());

  if (error) {
    throw error;
  }

  return {
    src,
    minified: Buffer.from(code)
  };
};

const elm_schema = {
  type: 'object',
  additionalProperties: false,
  properties: {
    entry: {
      type: 'string',
      required: true
    },
    output: {
      enum: [ 'inline', 'reference', 'static' ],
      required: true
    },
    target: {
      type: 'string',
      required: true
    },
    name: {
      type: 'string',
      required: true
    },
    public_path: {
      type: 'string',
      required: true
    },
    optimize: {
      type: 'bool',
      required: true
    },
    debug: {
      type: 'bool',
      required: true
    }
  }
};

const elmCompiler = async (entry, target, debug, optimize) => {
  const compilation_path = path.join(target, 'elm.js');

  await mkdirp(target);

  let cmd = `elm make ${entry} --output=${compilation_path}`;

  if (debug) {
    cmd = `elm make ${entry} --output=${compilation_path} --debug`;
  }
  else if (optimize) {
    cmd = `elm make ${entry} --output=${compilation_path} --optimize`;
  }

  try {
    await exec(cmd);
  }
  catch (e) {
    throw new Error(e.message);
  }

  const src = await readFile(compilation_path);

  try {
    await unlink(compilation_path);
  }
  catch (e) {}

  return src;
};

const compileElm = async (opts, compiler = elmCompiler) => {
  try {
    v.validate(opts, elm_schema, { throwError: true });
  }
  catch (e) {
    throw new Error(`validation errors: ${e.stack}`);
  }

  const { target, entry, name, output, public_path, debug, optimize } = opts;
  const file_exists = await exists(entry);

  if (!file_exists) {
    throw new Error('entry file does not exist');
  }

  let src = await compiler(entry, target, debug, optimize);

  if (optimize) {
    const compress_response = UglifyJS.minify(src.toString(), {
      mangle: false,
      compress: {
        drop_console: true,
        drop_debugger: true,
        pure_funcs: [ 'F2', 'F3', 'F4', 'F5', 'F6', 'F7', 'F8', 'F9', 'A2', 'A3', 'A4', 'A5', 'A6', 'A7', 'A8', 'A9' ],
        pure_getters: true,
        keep_fargs: false,
        unsafe_comps: true,
        unsafe: true
      }
    });

    if (compress_response.error) {
      throw compress_response.error;
    }

    const mangle_response = UglifyJS.minify(src.toString(), {
      mangle: true
    });

    if (mangle_response.error ) {
      throw mangle_response.error;
    }

    src = Buffer.from(mangle_response.code);
  }

  const filename = fileName(name, src, 'js');
  const url = path.join(public_path, filename);

  return {
    type: 'js',
    output,
    filename,
    src,
    url,
    target
  };
};

const js_schema = {
  type: 'object',
  additionalProperties: false,
  properties: {
    entry: {
      type: 'string',
      required: true
    },
    output: {
      enum: [ 'inline', 'reference', 'static' ],
      required: true
    },
    target: {
      type: 'string',
      required: true
    },
    name: {
      type: 'string',
      required: true
    },
    public_path: {
      type: 'string',
      required: true
    },
    optimize: {
      type: 'bool',
      required: true
    },
    hash: {
      type: 'bool',
      required: false
    }
  }
};
const loadJs = async (opts) => {
  try {
    v.validate(opts, js_schema, { throwError: true });
  }
  catch (e) {
    throw new Error(`validation errors: ${e.stack}`);
  }

  const { target, entry, name, output, public_path, optimize } = opts;
  const hash_filename = opts.hash === true || opts.hash === undefined;

  if (optimize) {
    const { minified } = await minifyJs(entry);
    const filename = hash_filename ? fileName(name, minified, 'js') : `${name}.js`;
    const url = path.join(public_path, filename);

    return {
      type: 'js',
      output,
      filename,
      src: minified,
      url,
      target
    };
  }
  else {
    const src = await readFile(entry);
    const filename = hash_filename ? fileName(name, src, 'js') : `${name}.js`;
    const url = path.join(public_path, filename);

    return {
      type: 'js',
      output,
      filename,
      src,
      url,
      target
    };
  }
};

module.exports = {
  loadJs,
  compileElm,
  minifyJs
};
