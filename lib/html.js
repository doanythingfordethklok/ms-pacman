const { promisify } = require('util');
const readFile = promisify(require('fs').readFile);
const { Validator } = require('jsonschema');
const v = new Validator();

const schema = {
  type: 'object',
  additionalProperties: false,
  properties: {
    entry: {
      type: 'string',
      required: true
    },
    target: {
      type: 'string',
      required: true
    },
    name: {
      type: 'string',
      required: true
    }
  }
};

const injectResourcesToHtml = async (opts, resources) => {
  try {
    v.validate(opts, schema, { throwError: true });
  }
  catch (e) {
    throw new Error(`validation errors: ${e.stack}`);
  }

  const { entry, target, name } = opts;
  const html = await readFile(entry);
  const css = resources
    .filter((r) => r.type === 'css')
    .map(({ url, output, src }) => {
      if (output === 'inline') {
        return `<style type="text/css">${src}</style>`;
      }
      else if (output === 'reference') {
        return `<link rel="stylesheet" type="text/css" href="${url}" />`;
      }

      return '';
    })
    .join('');

  const js = resources
    .filter((r) => r.type === 'js')
    .map(({ url, output, src }) => {
      if (output === 'inline') {
        return `<script type="text/javascript">${src}</script>`;
      }
      else if (output === 'reference') {
        return `<script src="${url}" type="text/javascript"></script>`;
      }

      return '';
    })
    .join('');

  const src = Buffer.from(eval('`' + html + '`'));
  const filename = `${name}.html`;
  const url = `${name}.html`;

  return {
    type: 'html',
    filename,
    src,
    url,
    target
  };
};

module.exports = { injectResourcesToHtml };
