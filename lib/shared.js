const md5 = require('md5');

const fileName = (name, src, ext) => `${name}-${md5(src)}.${ext}`;

module.exports = { fileName };
