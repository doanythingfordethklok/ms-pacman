const path = require('path');
const { promisify } = require('util');
const mkdirp = promisify(require('mkdirp'));
const writeFile = promisify(require('fs').writeFile);

const writeFilesToFileSystem = async (files, make_directory = mkdirp, write_the_file = writeFile) => {
  const ops = files.map(async ({ target, filename, src, output }) => {
    if (output !== 'inline') {
      await make_directory(target);
      await write_the_file(path.join(target, filename), src);
    }
  });

  return await Promise.all(ops);
};

// return an object indexed by the url of the file.
const writeFilesToObject = async (files) => {
  return files.reduce((acc, file) => {
    if (file.output !== 'inline') {
      return {
        ...acc,
        [file.url]: file
      };
    }

    return acc;
  }, {});
};

module.exports = { writeFilesToFileSystem, writeFilesToObject };
