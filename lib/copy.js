const path = require('path');
const { promisify } = require('util');
const readFile = promisify(require('fs').readFile);
const { Validator } = require('jsonschema');
const v = new Validator();

const schema = {
  type: 'object',
  additionalProperties: false,
  properties: {
    entry: {
      type: 'string',
      required: true
    },
    target: {
      type: 'string',
      required: true
    },
    filename: {
      type: 'string',
      required: true
    },
    public_path: {
      type: 'string',
      required: true
    }
  }
};

const copyFile = async (opts) => {
  try {
    v.validate(opts, schema, { throwError: true });
  } catch (e) {
    throw new Error(`validation errors: ${e.stack}`);
  }

  const { entry, target, filename, public_path } = opts;
  const src = await readFile(entry);
  const url = path.join(public_path, filename);

  return {
    type: 'file',
    filename,
    src,
    url,
    target
  };
};

module.exports = { copyFile };
