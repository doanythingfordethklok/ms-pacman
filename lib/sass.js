const path = require('path');
const { promisify } = require('util');
const compileSASS = promisify(require('node-sass').render);
const autoprefixer = require('autoprefixer');
const postcss = require('postcss');
const cssnano = require('cssnano');
const { fileName } = require('./shared.js');
const { Validator } = require('jsonschema');
const v = new Validator();

const schema = {
  type: 'object',
  additionalProperties: false,
  properties: {
    entry: {
      type: 'string',
      required: true
    },
    output: {
      enum: [ 'inline', 'reference', 'static' ],
      required: true
    },
    target: {
      type: 'string',
      required: true
    },
    name: {
      type: 'string',
      required: true
    },
    public_path: {
      type: 'string',
      required: true
    },
    includePaths: {
      type: 'array',
      required: true,
      items: { type: 'string' }
    }
  }
};

const POST_CSS_PLUGINS = [
  autoprefixer({
    grid: true,
    add: true,
    remove: true,
    browsers: [ '>1%' ]
  }),
  cssnano({
    preset: [
      'default',
      {
        discardComments: {
          removeAll: true
        }
      }
    ]
  })
];

const compileSass = async (opts) => {
  try {
    v.validate(opts, schema, { throwError: true });
  }
  catch (e) {
    throw new Error(`validation errors: ${e.stack}`);
  }

  const { target, name, entry, includePaths, output, public_path } = opts;
  const sass_result = await compileSASS({
    file: entry,
    includePaths
  });
  const css_result = await postcss(POST_CSS_PLUGINS).process(sass_result.css, { from: undefined });
  const src = Buffer.from(css_result.css);
  const filename = fileName(name, src, 'css');
  const url = path.join(public_path, filename);

  return {
    type: 'css',
    output,
    filename,
    src,
    url,
    target
  };
};

module.exports = { compileSass };
