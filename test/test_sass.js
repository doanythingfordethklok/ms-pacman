const test = require('tape');
const { compileSass } = require('../lib/sass.js');

test('Sass: happy path', (t) => {
  const opts = {
    output: 'static',
    entry: require.resolve('./files/test.sass'),
    name: 'rick-james',
    includePaths: [],
    target: '/path-to-directory',
    public_path: '/burp'
  };

  compileSass(opts)
    .then((result) => {
      t.equal(result.type, 'css', 'file type');
      t.equal(result.output, opts.output, 'output');
      t.ok(/^rick-james-(\w*)\.css$/.test(result.filename), 'file name');
      t.ok(result.src.toString().length > 0, 'src not empty');
      t.ok(/^\/burp\/rick-james-(\w*)\.css$/.test(result.url), 'correct url');
      t.equal(result.target, opts.target, 'correct target');
      t.end();
    })
    .catch((e) => {
      t.fail(e);
    });
});

test('Sass: bad config', (t) => {
  const opts = {};

  compileSass(opts)
    .then(() => {
      t.fail(new Error('should have failed'));
    })
    .catch((e) => {
      t.ok(e.message.startsWith('validation errors'), 'correct error message');
      t.end();
    });
});

test('Sass: file not found', (t) => {
  const opts = {
    entry: 'jshdfklshfkls',
    output: 'reference',
    name: 'rick-james',
    includePaths: [],
    target: '/path-to-directory',
    public_path: '/burp'
  };

  compileSass(opts)
    .then(() => {
      t.fail(new Error('should have failed'));
    })
    .catch((e) => {
      t.ok(e.message.startsWith('File to read not found or unreadable'), 'correct error message');
      t.end();
    });
});
