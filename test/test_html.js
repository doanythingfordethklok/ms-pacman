const test = require('tape');
const cheerio = require('cheerio');
const { injectResourcesToHtml } = require('../lib/html.js');

test('Html: no resources', (t) => {
  const opts = {
    entry: require.resolve('./files/test.html'),
    target: '/path-to-directory',
    name: 'shred'
  };
  const resources = [];

  injectResourcesToHtml(opts, resources)
    .then((result) => {
      t.equal(result.type, 'html', 'file type');
      t.equal(result.filename, 'shred.html', 'file name');
      t.equal(result.url, 'shred.html', 'correct url');
      t.equal(result.target, opts.target, 'correct target');
      t.equal(result.src.toString(), '<html><head></head><body></body></html>', 'correct src');
      t.end();
    })
    .catch((e) => {
      t.fail(e);
    });
});

test('Html: inject inline and refs', (t) => {
  const opts = {
    entry: require.resolve('./files/test.html'),
    target: '/path-to-directory',
    name: 'shred'
  };

  const js_inline = {
    type: 'js',
    output: 'inline',
    src: Buffer.from('alert();'),
    url: '/js1'
  };
  const js_ref = {
    type: 'js',
    output: 'reference',
    src: Buffer.from('console.log(420);'),
    url: '/js2'
  };
  const js_static = {
    type: 'js',
    output: 'static',
    src: Buffer.from('console.log(69);'),
    url: '/js2'
  };
  const css_inline = {
    type: 'css',
    output: 'inline',
    src: Buffer.from('body{color:red;}'),
    url: '/css1'
  };
  const css_ref = {
    type: 'css',
    output: 'reference',
    src: Buffer.from('body{color:yellow;}'),
    url: '/css2'
  };
  const css_static = {
    type: 'css',
    output: 'static',
    src: Buffer.from('body{color:yellow;}'),
    url: '/css2'
  };
  const resources = [ js_inline, js_ref, js_static, css_inline, css_ref, css_static ];

  injectResourcesToHtml(opts, resources)
    .then((result) => {
      t.equal(result.type, 'html', 'file type');
      t.equal(result.filename, 'shred.html', 'file name');
      t.equal(result.url, 'shred.html', 'correct url');
      t.equal(result.target, opts.target, 'correct target');

      const $ = cheerio.load(result.src);

      t.equal($('style').length, 1, 'css inline: correct count');
      t.equal($('style').attr('type'), 'text/css', 'css inline: correct type');
      t.equal($('style').html(), css_inline.src.toString(), 'css inline: correct src');

      t.equal($('link').length, 1, 'css ref: correct count');
      t.equal($('link').attr('type'), 'text/css', 'css ref: correct type');
      t.equal($('link').attr('rel'), 'stylesheet', 'css ref: correct rel');
      t.equal($('link').attr('href'), css_ref.url, 'css ref: correct href');
      t.equal($('link').html(), '', 'css ref: correct src');

      t.equal($('script').length, 2, 'js: correct tag count');

      const tag_js_inline = $('script').filter(function (i, el) {
        return !$(el).attr('src');
      });
      const tag_js_ref = $('script').filter(function (i, el) {
        return $(el).attr('src');
      });

      t.equal(tag_js_inline.attr('type'), 'text/javascript', 'js inline: correct type');
      t.equal(tag_js_inline.html(), js_inline.src.toString(), 'js inline: correct src');
      t.equal(tag_js_ref.attr('type'), 'text/javascript', 'js ref: correct type');
      t.equal(tag_js_ref.attr('src'), js_ref.url, 'js ref: correct src');
      t.equal(tag_js_ref.html(), '', 'js ref: no inner script');

      t.end();
    })
    .catch((e) => {
      t.fail(e);
    });
});

test('Html: bad config', (t) => {
  const opts = {};

  injectResourcesToHtml(opts, [])
    .then(() => {
      t.fail(new Error('should have failed'));
    })
    .catch((e) => {
      t.ok(e.message.startsWith('validation errors'), 'correct error message');
      t.end();
    });
});

test('Html: file not found', (t) => {
  const opts = {
    entry: 'jsdhflsd',
    target: 'sdkjfh',
    name: 'index'
  };

  injectResourcesToHtml(opts, [])
    .then(() => {
      t.fail(new Error('should have failed'));
    })
    .catch((e) => {
      t.ok(e.message.startsWith('ENOENT'), 'correct error message');
      t.end();
    });
});
