const test = require('tape');
const { compileElm } = require('../lib/script.js');
const path = require('path');

test('Elm: happy path', (t) => {
  const opts = {
    entry: require.resolve('./files/Main.elm'),
    output: 'static',
    name: 'oathbreaker',
    target: path.resolve(path.join(__dirname, '../tmp')),
    public_path: '/burp',
    optimize: false,
    debug: false
  };
  const fake_compiler = async (entry, target, debug, optimize) => {
    t.equal(entry, opts.entry, 'compiler: entry');
    t.equal(target, opts.target, 'compiler: target');
    t.equal(debug, opts.debug, 'compiler: debug');
    t.equal(optimize, opts.optimize, 'compiler: optimize');

    return 'var x = true;';
  };

  compileElm(opts, fake_compiler)
    .then((result) => {
      t.equal(result.type, 'js', 'file type');
      t.equal(result.output, opts.output, 'output');
      t.ok(/^oathbreaker-(\w*)\.js$/.test(result.filename), 'file name');
      t.ok(result.src.toString().length > 0, 'src not empty');
      t.ok(/^\/burp\/oathbreaker-(\w*)\.js$/.test(result.url), 'correct url');
      t.equal(result.target, opts.target, 'correct target');

      if (t.assertCount !== 10) {
        t.fail(`assertion count incorrect. ${t.assertCount} out of ${number}`);
      }

      t.end();
    })
    .catch((e) => {
      console.log(e);
      t.fail(e);
    });
});

test('Elm: bad config', (t) => {
  const opts = {};

  compileElm(opts)
    .then(() => {
      t.fail(new Error('should have failed'));
    })
    .catch((e) => {
      t.ok(e.message.startsWith('validation errors'), 'correct error message');
      t.end();
    });
});

test('Elm: file not found', (t) => {
  const opts = {
    entry: 'jshdfklshfkls',
    output: 'static',
    name: 'appleseed-cast',
    target: path.resolve(path.join(__dirname, '../tmp')),
    public_path: '/burp',
    optimize: true,
    debug: false
  };

  compileElm(opts)
    .then(() => {
      t.fail(new Error('should have failed'));
    })
    .catch((e) => {
      console.log(e.message);

      t.equal(e.message, 'entry file does not exist', 'correct error message');
      t.end();
    });
});
