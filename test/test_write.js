const test = require('tape');
const { writeFilesToObject, writeFilesToFileSystem } = require('../lib/write.js');

test('Write Object: happy path', (t) => {
  const files = [
    {
      url: '/beep.html'
    },
    {
      url: '/inline.css',
      output: 'inline'
    },
    {
      url: '/ref.css',
      output: 'reference'

    },
    {
      url: '/inline.js',
      output: 'inline'
    },
    {
      url: '/ref.js',
      output: 'reference'
    },
    {
      url: '/static.js',
      output: 'static'
    }
  ];

  writeFilesToObject(files)
    .then((result) => {
      t.equal(Object.keys(result).length, 4, 'correct number of files');
      t.deepEqual(result['/beep.html'], files[0], 'beep in result');
      t.deepEqual(result['/ref.css'], files[2], 'ref.css in result');
      t.deepEqual(result['/ref.js'], files[4], 'ref.js in result');
      t.deepEqual(result['/static.js'], files[5], 'static.js in result');

      t.end();
    })
    .catch((e) => {
      t.fail(e);
    });
});

test('Write Files: happy path', (t) => {
  const target = '/fake-path';

  const files = [
    {
      target,
      filename: 'beep.html',
      src: Buffer.from('beep')
    },
    {
      target,
      filename: 'bop.css',
      output: 'inline',
      src: Buffer.from('bop')
    },
    {
      target,
      filename: 'bork.css',
      output: 'reference',
      src: Buffer.from('bork')
    },
    {
      target,
      filename: 'fork.js',
      output: 'inline',
      src: Buffer.from('fork')
    },
    {
      target,
      filename: 'mork.js',
      output: 'reference',
      src: Buffer.from('mork')
    }
  ];

  const mkdirp = async (dir) => {
    t.equal(dir, target, 'correct mkdirp dir');
  };

  const writeFile = async (path, src) => {
    console.log(path, src);

    if (path === '/fake-path/beep.html') {
      t.equal(src, files[0].src, 'correct src for beep');
    }

    if (path === '/fake-path/bork.css') {
      t.equal(src, files[2].src, 'correct src for beep');
    }

    if (path === '/fake-path/mork.js') {
      t.equal(src, files[4].src, 'correct src for beep');
    }
  };

  writeFilesToFileSystem(files, mkdirp, writeFile)
    .then((result) => {
      if (t.assertCount !== 6) {
        t.fail(`assertion count incorrect. ${t.assertCount} out of ${number}`);
      }

      t.end();
    })
    .catch((e) => {
      t.fail(e);
    });
});
