const test = require('tape');
const { loadJs } = require('../lib/script.js');

test('JS: happy path', (t) => {
  const opts = {
    output: 'inline',
    entry: require.resolve('./files/test.js'),
    name: 'appleseed-cast',
    target: '/path-to-directory',
    public_path: '/burp',
    optimize: true
  };

  const correct_src = `x&&alert(x);`;

  loadJs(opts)
    .then((result) => {
      console.log(result.src.toString());
      t.equal(result.type, 'js', 'file type');
      t.equal(result.output, opts.output, 'output');
      t.ok(/^appleseed-cast-(\w*)\.js$/.test(result.filename), 'file name');
      t.equal(result.src.toString(), correct_src, 'src is compressed');
      t.ok(/^\/burp\/appleseed-cast-(\w*)\.js$/.test(result.url), 'correct url');
      t.equal(result.target, opts.target, 'correct target');
      t.end();
    })
    .catch((e) => {
      t.fail(e);
    });
});

test('JS: happy path - no optimize', (t) => {
  const opts = {
    output: 'inline',
    entry: require.resolve('./files/test.js'),
    name: 'appleseed-cast',
    target: '/path-to-directory',
    public_path: '/burp',
    optimize: false
  };

  const correct_src = `if (x) {
  alert(x);
}
`;

  loadJs(opts)
    .then((result) => {
      t.equal(result.type, 'js', 'file type');
      t.equal(result.output, opts.output, 'output');
      t.ok(/^appleseed-cast-(\w*)\.js$/.test(result.filename), 'file name');
      t.equal(result.src.toString(), correct_src, 'src is correct');
      t.ok(/^\/burp\/appleseed-cast-(\w*)\.js$/.test(result.url), 'correct url');
      t.equal(result.target, opts.target, 'correct target');
      t.end();
    })
    .catch((e) => {
      t.fail(e);
    });
});

test('JS: bad config', (t) => {
  const opts = {};

  loadJs(opts)
    .then(() => {
      t.fail(new Error('should have failed'));
    })
    .catch((e) => {
      t.ok(e.message.startsWith('validation errors'), 'correct error message');
      t.end();
    });
});

test('JS filename: hash: false, optimize: true', (t) => {
  const opts = {
    output: 'inline',
    entry: require.resolve('./files/test.js'),
    name: 'dont-change',
    target: '/path-to-directory',
    public_path: '/burp',
    optimize: true,
    hash: false
  };

  loadJs(opts)
    .then((result) => {
      t.equal(result.filename, 'dont-change.js',  'file name - no md5 hash');
      t.equal(result.url, '/burp/dont-change.js', 'correct url - no md5 hash');
      t.end();
    })
    .catch((e) => {
      t.fail(e);
    });
});

test('JS filename: hash: false, optimize: false', (t) => {
  const opts = {
    output: 'inline',
    entry: require.resolve('./files/test.js'),
    name: 'dont-change',
    target: '/path-to-directory',
    public_path: '/burp',
    optimize: false,
    hash: false
  };

  loadJs(opts)
    .then((result) => {
      t.equal(result.filename, 'dont-change.js',  'file name - no md5 hash');
      t.equal(result.url, '/burp/dont-change.js', 'correct url - no md5 hash');
      t.end();
    })
    .catch((e) => {
      t.fail(e);
    });
});

test('JS: file not found', (t) => {
  const opts = {
    entry: 'jshdfklshfkls',
    output: 'static',
    name: 'appleseed-cast',
    target: '/path-to-directory',
    public_path: '/burp',
    optimize: true
  };

  loadJs(opts)
    .then(() => {
      t.fail(new Error('should have failed'));
    })
    .catch((e) => {
      t.ok(e.message.startsWith('ENOENT'), 'correct error message');
      t.end();
    });
});
