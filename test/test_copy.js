const test = require('tape');
const { copyFile } = require('../lib/copy.js');

test('Copy: happy path', (t) => {
  const opts = {
    entry: require.resolve('./files/test.txt'),
    filename: 'lip-gloss.txt',
    target: '/path-to-directory',
    public_path: '/burp'
  };

  copyFile(opts)
    .then((result) => {
      t.equal(result.type, 'file', 'file type');
      t.equal(result.filename, opts.filename, 'file name');
      t.equal(result.src.toString(), 'hi', 'src');
      t.equal(result.url, '/burp/lip-gloss.txt', 'correct url');
      t.equal(result.target, opts.target, 'correct target');
      t.end();
    })
    .catch((e) => {
      t.fail(e);
    });
});

test('Copy: bad config', (t) => {
  const opts = {};

  copyFile(opts)
    .then(() => {
      t.fail(new Error('should have failed'));
    })
    .catch((e) => {
      t.ok(e.message.startsWith('validation errors'), 'correct error message');
      t.end();
    });
});

test('Copy: file not found', (t) => {
  const opts = {
    entry: 'jshdfklshfkls',
    filename: 'lip-gloss.txt',
    target: '/path-to-directory',
    public_path: '/burp'
  };

  copyFile(opts)
    .then(() => {
      t.fail(new Error('should have failed'));
    })
    .catch((e) => {
      t.ok(e.message.startsWith('ENOENT'), 'correct error message');
      t.end();
    });
});
