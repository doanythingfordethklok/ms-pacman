const { copyFile } = require('./lib/copy.js');
const { injectResourcesToHtml } = require('./lib/html.js');
const { compileSass } = require('./lib/sass.js');
const { compileElm, loadJs } = require('./lib/script.js');
const { writeFilesToFileSystem, writeFilesToObject } = require('./lib/write.js');
const chokidar = require('chokidar');

const buildHtml = async (html_steps, resources) => {
  const html_ops = html_steps.map((html) => injectResourcesToHtml(html, resources));
  return await Promise.all(html_ops);
};

const buildResources = async (build_steps) => {
  const ops = build_steps.map((task) => {
    switch (task.type) {
      case 'sass':
        return compileSass(task.options);

      case 'elm':
        return compileElm(task.options);

      case 'js':
        return loadJs(task.options);

      case 'copy':
        return copyFile(task.options);

      default:
        console.log(`bad config: ${task.type} not supported`);
        process.exit(1);
    }
  });

  return await Promise.all(ops);
};

const buildAll = async (build_steps, html_steps) => {
  const resources = await buildResources(build_steps);
  const html_resources = await buildHtml(html_steps, resources);

  return resources.concat(html_resources);
};

const devMiddleware = (build_steps, html_steps, watches) => {
  let file_system = null;
  let latest_version = null;
  let current_timeout = null;

  const rebuild = async (uid) => {
    if (latest_version !== uid) {
      return;
    }

    console.log('Rebuilding...');
    const files = await buildAll(build_steps, html_steps);
    const fs = await writeFilesToObject(files);

    if (latest_version === uid) {
      file_system = fs;
    }

    files.forEach((f) => console.log({ ...f, src: '[...]' }));
    console.log('Everything is up to date.');
  };

  chokidar.watch(watches, { ignored: /(^|[\/\\])\../ }).on('all', (event, path) => {
    console.log(event, path);

    latest_version = Date.now();

    clearTimeout(current_timeout);
    current_timeout = setTimeout(
      () =>
        rebuild(latest_version).catch((e) => {
          console.log(e);
        }),
      800
    );
  });

  return (req, res, next) => {
    if (file_system === null) {
      next(new Error('Resources are not built.'));
      return;
    }

    if (file_system[req.url]) {
      res.type(file_system[req.url].filename);
      res.send(file_system[req.url].src);
      res.end();
    }
    else {
      res.set('content-type', 'text/html');
      res.send(file_system['index.html'].src);
      res.end();
    }
  };
};

module.exports = {
  buildResources,
  buildHtml,
  buildAll,
  writeFilesToFileSystem,
  writeFilesToObject,
  devMiddleware
};
